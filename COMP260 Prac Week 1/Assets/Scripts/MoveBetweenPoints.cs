﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {

	// Public Members
	public Vector3 startPoint, endPoint;
	public float speed = 1.0f;
	//Private Members 
	bool movingForward = true;

	// Use this for initialization
	void Start () {
		transform.position = startPoint;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target;
		if (movingForward) {
			target = endPoint;
		} else {
			target = startPoint;
		}
		float distanceToMove = speed * Time.deltaTime;
		float distanceToTarget = (target - transform.position).magnitude;
		Debug.Log ("distanceToTarget = " + distanceToTarget.ToString());
		if (distanceToMove > distanceToTarget) {
			transform.position = target;
			movingForward = !(movingForward);
		} else {
			transform.position += (target - transform.position).normalized * distanceToMove;
		}
	}
}
